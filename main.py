import re
import tweepy
import nltk
import pandas as pd
import matplotlib.pyplot as plt
from wordcloud import WordCloud, STOPWORDS
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer
from tweepy import OAuthHandler
from textblob import TextBlob

nltk.download('punkt')
nltk.download('stopwords')


def connect():
    # Replace the xxxxx with your twitter api keys
    consumer_key = "8nwz0CeOTx0diZah5xOErvxBg"
    consumer_secret = "CtbpS6RcuAlkyZuAcvwIRx9cZQssvolRRePpCWRBrHIYIiELvg"
    access_token = "1517109136479117313-oKEm4Hf27OfguYvOXavXCxaPpVep3n"
    access_token_secret = "oqA9HPYwxU7bnSZA268cQm8EHOWnHCDvgPNi2e9nO8LI8"

    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth)
    return api


def clean_text(text):
    text = text.lower()
    # Removes all mentions (@username) from the tweet since it is of no use to us
    text = re.sub(r'(@[A-Za-z0-9_]+)', '', text)

    # Removes any link in the text
    text = re.sub('http://\S+|https://\S+', '', text)

    # Only considers the part of the string with char between a to z or digits and whitespace characters
    # Basically removes punctuation
    text = re.sub(r'[^\w\s]', '', text)

    # Removes stop words that have no use in sentiment analysis
    text_tokens = word_tokenize(text)
    text = [word for word in text_tokens if not word in stopwords.words()]

    text = ' '.join(text)
    return text


def stem(text):
    # This function is used to stem the given sentence
    porter = PorterStemmer()
    token_words = word_tokenize(text)
    stem_sentence = []
    for word in token_words:
        stem_sentence.append(porter.stem(word))
    return " ".join(stem_sentence)


def sentiment(cleaned_text):
    # Returns the sentiment based on the polarity of the input TextBlob object
    if cleaned_text.sentiment.polarity > 0:
        return 'positive'
    elif cleaned_text.sentiment.polarity < 0:
        return 'negative'
    else:
        return 'neutral'


def fetch_tweets(query, count=50):
    api = connect()  # Gets the tweepy API object
    tweets = []  # Empty list that stores all the tweets

    # Fetches the tweets using the api
    fetched_data = api.search_tweets(q=query + ' -filter:retweets', count=count)
    for tweet in fetched_data:
        txt = tweet.text
        clean_txt = clean_text(txt)  # Cleans the tweet
        stem_txt = TextBlob(stem(clean_txt))  # Stems the tweet
        sent = sentiment(stem_txt)  # Gets the sentiment from the tweet
        tweets.append((txt, clean_txt, sent))
    return tweets


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    tweets = fetch_tweets(query='#JohnnyDepp', count=100)
    # Converting the list into a pandas Dataframe
    df = pd.DataFrame(tweets, columns=['tweets', 'clean_tweets', 'sentiment'])

    # Dropping the duplicate values just in case there are some tweets that are copied and then stores the data in a csv file
    df = df.drop_duplicates(subset='clean_tweets')
    df.to_csv('data.csv', index=False)
    ptweets = df[df['sentiment'] == 'positive']
    p_perc = 100 * len(ptweets) / len(tweets)
    ntweets = df[df['sentiment'] == 'negative']
    n_perc = 100 * len(ntweets) / len(tweets)
    print(f'Positive tweets {p_perc} %')
    print(f'Neutral tweets {100 - p_perc - n_perc} %')
    print(f'Negative tweets {n_perc} %')

    twt = " ".join(df['clean_tweets'])
    wordcloud = WordCloud(stopwords=STOPWORDS, background_color='black', width=2500, height=2000).generate(twt)

    plt.figure(1, figsize=(13, 13))
    plt.imshow(wordcloud)
    plt.axis('off')
    plt.show()
